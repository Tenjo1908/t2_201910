package api;

import model.data_structures.LinkedList;
import model.data_structures.Multa;
import model.vo.VOMovingViolations;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IMovingViolationsManager {

	/**
	 * Method to load the Moving Violations of the STS
	 * @param movingViolationsFile - path to the file 
	 */
	void loadMovingViolations(String movingViolationsFile);
	
	public LinkedList<Multa> getMovingViolationsByViolationCode (String violationCode);
	
	
	public LinkedList <Multa> getMovingViolationsByAccident(String accidentIndicator);

	
}
