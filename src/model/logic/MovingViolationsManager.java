package model.logic;

import java.io.FileNotFoundException;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.LinkedList;
import model.data_structures.Multa;

public class MovingViolationsManager implements IMovingViolationsManager {
	
	public LinkedList<Multa> infracciones;

	
	public void loadMovingViolations(String movingViolationsFile){
		// TODO Auto-generated method stub
		FileReader reader;
		infracciones=new LinkedList<>();
		try {
			reader = new FileReader(movingViolationsFile);
			@SuppressWarnings({ "deprecation", "resource" })
			CSVReader lectorCSV= new CSVReader(reader,';');
			String[] nextLine;
			nextLine=lectorCSV.readNext();
			
			while((nextLine=lectorCSV.readNext())!=null)
			{
				System.out.println(Integer.parseInt(nextLine[0])+nextLine[1]);
				Multa infraccion=new Multa(Integer.parseInt(nextLine[0]),nextLine[1], nextLine[2], nextLine[3], nextLine[4], nextLine[5], nextLine[6], Integer.parseInt(nextLine[7]), Integer.parseInt(nextLine[8]), Integer.parseInt(nextLine[9]), nextLine[11], nextLine[12], nextLine[13], nextLine[14]);
				infracciones.add(infraccion);
			}
					
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
	}

		
	@Override
	public LinkedList <Multa> getMovingViolationsByViolationCode (String violationCode) {
		// TODO Auto-generated method stub
		LinkedList<Multa> violaciones= new LinkedList<>();
		int i=0;
		while(infracciones.get(i)!=null)
		{
			if(infracciones.get(i).getViolationCode().equals(violationCode))
			{
				violaciones.add(infracciones.get(i));
				i++;
			}
		}
		return violaciones;
	}

	@Override
	public LinkedList <Multa> getMovingViolationsByAccident(String accidentIndicator) {
		// TODO Auto-generated method stub
		LinkedList<Multa> violaciones= new LinkedList<>();
		int i=0;
		while(infracciones.get(i)!=null)
		{
			if(infracciones.get(i).getAccidentIndicator().equals(accidentIndicator))
			{
				violaciones.add(infracciones.get(i));
				i++;
			}
		}
		return violaciones;
	}	


}

