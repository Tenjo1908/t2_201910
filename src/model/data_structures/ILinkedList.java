package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T> extends Iterable<T> {
	
	
	public boolean add(T elemento);
	
	public void addAtEnd(T elemento);
	
	public void AddAtK(T elemento, int pos);
	
	public T getElement();
	
	public T getCurrentElement(int k);
	
	public void delete();
	
	public void deleteAtk(int k);
	
	public T next();
	
	public T previous();
	
	

	Integer getSize();

}
