package model.data_structures;

public class Multa 
{
	private int objectID;
	
	private String location;
	
	private String addressID;
	
	private String streetSEGID;
	
	private String xCoord;
	
	private String yCoord;
	
	private String ticketTipe;
	
	private int fineAmt;
	
	private int totalPaid;
	
	private int penalty1;
	
	
	private String accidentIndicator;
	
	private String ticketIssueDate;
	
	private String violationCode;
	
	private String violationDesc;
	
	
	public Multa(int pObjectID, String pLocation, String pAddressID, String pStreetSEGID, String pxCoord, String pyCoord, String pTicketTipe, int pFineAmt, int pTotalPaid, int pPenalty1, String pAccidentIndicator, String pTicketIssueDate, String pViolationCode, String pViolationDesc)
	{
		
		objectID= pObjectID;
		location=pLocation;
		setAddressID(pAddressID);
		setStreetSEGID(pStreetSEGID);
		setxCoord(pxCoord);
		setyCoord(pyCoord);
		setTicketIssueDate(pTicketIssueDate);
		setTicketTipe(pTicketTipe);
		setFineAmt(pFineAmt);
		setTotalPaid(pTotalPaid);
		setPenalty1(pPenalty1);
		setAccidentIndicator(pAccidentIndicator);
		setViolationCode(pViolationCode);
		setViolationDesc(pViolationDesc);
		
	}
	
	public int getObjectID()
	{
		return objectID;
	}
	public  String getLocation()
	{
		return location;
	}

	public String getAddressID() {
		return addressID;
	}

	public void setAddressID(String addressID) {
		this.addressID = addressID;
	}

	public String getStreetSEGID() {
		return streetSEGID;
	}

	public void setStreetSEGID(String streetSEGID) {
		this.streetSEGID = streetSEGID;
	}

	public String getxCoord() {
		return xCoord;
	}

	public void setxCoord(String xCoord) {
		this.xCoord = xCoord;
	}

	public String getyCoord() {
		return yCoord;
	}

	public void setyCoord(String yCoord) {
		this.yCoord = yCoord;
	}

	public String getTicketTipe() {
		return ticketTipe;
	}

	public void setTicketTipe(String ticketTipe) {
		this.ticketTipe = ticketTipe;
	}

	public int getFineAmt() {
		return fineAmt;
	}

	public void setFineAmt(int fineAmt) {
		this.fineAmt = fineAmt;
	}

	public int getTotalPaid() {
		return totalPaid;
	}

	public void setTotalPaid(int totalPaid) {
		this.totalPaid = totalPaid;
	}

	public int getPenalty1() {
		return penalty1;
	}

	public void setPenalty1(int penalty1) {
		this.penalty1 = penalty1;
	}

	public String getAccidentIndicator() {
		return accidentIndicator;
	}

	public void setAccidentIndicator(String accidentIndicator) {
		this.accidentIndicator = accidentIndicator;
	}

	public String getTicketIssueDate() {
		return ticketIssueDate;
	}

	public void setTicketIssueDate(String ticketIssueDate) {
		this.ticketIssueDate = ticketIssueDate;
	}

	public String getViolationCode() {
		return violationCode;
	}

	public void setViolationCode(String violationCode) {
		this.violationCode = violationCode;
	}

	public String getViolationDesc() {
		return violationDesc;
	}

	public void setViolationDesc(String violationDesc) {
		this.violationDesc = violationDesc;
	}
	
	
	

}
